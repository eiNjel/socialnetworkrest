# REST API for small Social Network app with jwt auth method.

### Skill example project

Email verification during registration goes trough hunter.io, 
and additional user information gets from clearbits enrichment 
if it is available. Both methods can be found inside utilities/services
folder and require only requests package to execute successfully.

Default authentication is json web token, library used
djangorestframework-jwt. Library had to be tweaked a bit to work under
email field as username field during login process. Package can be
found in custom_jwt/.

In order to increase speed performance while getting additional user data,
using enrichment, async/await method has been used.

### Testing
To test this project please use AutoBot project from my repo.
