# -*- coding: utf-8 -*-
import asyncio
import aiohttp
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


async def main(user_data):
    instance = user_data
    email = instance.email
    # https://dashboard.clearbit.com/docs?python#enrichment-api-combined-api
    # clearbit secret key is inside djangos settings so we're gonna try to get it
    try:
        key = settings.USER_SETTINGS['CLEARBIT_SK']
    except KeyError:
        raise KeyError(
            _('Clearbit api key is not present in django settings... Please add it')
        )
    # request data from clearbit enrichment type and return data in json format
    async with aiohttp.ClientSession(auth=aiohttp.BasicAuth(key, '')) as session:
        async with session.get('https://person.clearbit.com/v2/combined/find?email={}'.format(email)) as resp:
            enrich_details = await resp.json()

            full_name = None
            first_name = None
            last_name = None
            avatar = None
            location = None
            facebook = None
            twitter = None
            company_name = None

            if enrich_details:
                # handling key errors for users that has no details from clearbit enrichment
                try:
                    if enrich_details['person']:
                        full_name = enrich_details['person']['name']['fullName']
                        if ' ' in full_name:
                            first_name = full_name.split(' ')[0]
                            last_name = full_name.split(' ')[1]
                        if enrich_details['person']['avatar']:
                            avatar = enrich_details['person']['avatar']

                        if enrich_details['person']['location']:
                            location = enrich_details['person']['location']

                        if enrich_details['person']['facebook']['handle']:
                            facebook = 'https://www.facebook.com/' + str(enrich_details['person']['facebook']['handle'])

                        if enrich_details['person']['twitter']['handle']:
                            twitter = 'https://twitter.com/' + str(enrich_details['person']['twitter']['handle'])

                    if enrich_details['company']:
                        company_name = enrich_details['company']['name']
                except KeyError:
                    pass

            # setting data to registered user instance
            instance.full_name = full_name if full_name else ''
            instance.first_name = first_name if first_name else ''
            instance.last_name = last_name if last_name else ''
            instance.avatar = avatar if avatar else ''
            instance.location = location if location else ''
            instance.facebook = facebook if facebook else ''
            instance.twitter = twitter if twitter else ''
            instance.company_name = company_name if company_name else ''
            # saving new user data
            instance.save(update_fields=[
                'full_name', 'first_name', 'last_name', 'avatar', 'location', 'facebook', 'twitter', 'company_name'
            ])


def get_info(instance):
    # https://docs.djangoproject.com/en/2.0/ref/databases/#connection-management
    # since this method may raise errors due to opened db connections
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main(instance))
