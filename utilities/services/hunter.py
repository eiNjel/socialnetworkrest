# -*- coding: utf-8 -*-
import requests
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


def validate_email_(email):
    # https://hunter.io/api/v2/docs#email-verifier
    # hunter api key is withing django settings file so we're gonna try to fetch it
    try:
        key = settings.USER_SETTINGS['EMAIL_HUNTER_API_KEY']
    except KeyError:
        raise KeyError(
            _('Email hunter api key is not present in django settings... Please add it')
        )
    url = 'https://api.hunter.io/v2/email-verifier?email={email}&api_key={key}'.format(
        email=email,
        key=key
    )
    # request data and return in json format
    data = requests.get(url, stream=True)
    return data.json()
