# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from authentificate.models import user


class UserAdmin(admin.ModelAdmin):
    # simple display/filter list in admin
    list_display = ['email', 'username']
    list_filter = ['email', 'username', 'first_name', 'company_name']


admin.site.register(user, UserAdmin)
