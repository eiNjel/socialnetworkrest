# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2017-12-04 16:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='user',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('is_staff', models.BooleanField(default=False, verbose_name='Is Staff?')),
                ('is_superuser', models.BooleanField(default=False, verbose_name='Is SuperUser?')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Active?')),
                ('username', models.CharField(blank=True, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, verbose_name='Username: ')),
                ('email', models.EmailField(help_text='Required. Only accepts validated email addresses from emailhunter.', max_length=254, unique=True, verbose_name='Email: ')),
                ('first_name', models.CharField(blank=True, help_text='Users first name if provided, 64 chars or fewer.', max_length=64, verbose_name='First Name: ')),
                ('last_name', models.CharField(blank=True, help_text='Users last name if provided, 126 chars or fewer.', max_length=126, verbose_name='Last Name: ')),
                ('full_name', models.CharField(blank=True, help_text='Users full name if provided, 256 chars of fewer.', max_length=256, verbose_name='Full Name: ')),
                ('company_name', models.CharField(blank=True, help_text='Users company name if provided, 256 chars of fewer.', max_length=256, verbose_name='Company Name: ')),
                ('avatar', models.URLField(blank=True, help_text='Users avatar if provided, i didnt wanted to use image obj cuz right now, getting image via requests would be too much delay time. String URL can be used as trusted URL further in frontend', max_length=512, verbose_name='Avatar: ')),
                ('location', models.CharField(blank=True, help_text='Users location if provided, 256 chars of fewer.', max_length=256, verbose_name='Location: ')),
                ('facebook', models.URLField(blank=True, help_text='Users FB if provided, 512 chars or fewer.', max_length=512, verbose_name='FB Profile: ')),
                ('twitter', models.URLField(blank=True, help_text='Users Twitter if provided, 512 chars or fewer.', max_length=512, verbose_name='Twitter: ')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Account Created: ')),
                ('last_login', models.DateTimeField(auto_now=True, verbose_name='Last Login: ')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
        ),
    ]
