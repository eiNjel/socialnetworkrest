# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _


def jwt_encode(user):
    try:
        from custom_jwt.settings import api_settings
    except ImportError:
        raise ImportError(
            _('djangorestframework-jwt is not found but mandatory for this project. Please install it...')
        )

    # set handlers
    jwt_payload = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encoder = api_settings.JWT_ENCODE_HANDLER

    # load payload
    payload = jwt_payload(user)
    # return encoded payload
    return jwt_encoder(payload)
