# -*- coding: utf-8 -*-
import re
from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from django.contrib.auth import (
    get_user_model,
    authenticate
)
from utilities.services.clearbit import get_info
from utilities.services.hunter import validate_email_


User = get_user_model()


class UserDetailsSerializer(serializers.ModelSerializer):
    """ Normal model serializer excluding specific fields """

    class Meta:
        model = User
        read_only_fields = ['email']
        exclude = ['password', 'groups', 'user_permissions']


class UserRegistrationSerializer(serializers.Serializer):
    """ User registration serializer """

    email = serializers.EmailField(
        required=True
    )
    password = serializers.CharField(
        write_only=True
    )
    re_password = serializers.CharField(
        write_only=True
    )

    def validate_email(self, email):
        """ validate users email """

        # get email existence validation from hunter
        email_exists = validate_email_(email)
        #
        # # better safe than sorry
        if re.search(r'undeliverable', email_exists['data']['result']) or not email_exists['data']['smtp_check']:
            raise forms.ValidationError(
                _('Sooorry, but this email doesn\'t seem to exist.')
            )

        # check if email is already registered, emails are unique per user
        check_email = User.objects.filter(email=email).exists()

        if check_email:
            raise forms.ValidationError(
                _('This email is already registered.')
            )
        return email

    def validate_password(self, password, user=None):
        """ validate users password """
        # checking minimum pw length from settings with length of provided pw
        min_pw_length = settings.USER_SETTINGS['PASSWORD_MIN_LENGTH']

        if min_pw_length and (len(password) < min_pw_length):
            raise forms.ValidationError(
                _('Password must be a minimum of {} characters.').format(min_pw_length)
            )
        validate_password(password, user)
        return password

    def validate(self, attrs):
        """ check password match """
        if attrs['password'] != attrs['re_password']:
            raise forms.ValidationError(
                _('Passwords doesn\'t match.')
            )
        return attrs

    def get_validated_data(self):

        email = self.validated_data.get('email')
        password = self.validated_data.get('password')

        user_details = {
            'email': email,
            'password': password,
        }

        return user_details

    def create(self, validated_data):
        data = self.get_validated_data()
        # creating user with validated data if all conditions are met
        user = User.objects.create_user(**data)
        get_info(user)
        return user


class LoginSerializer(serializers.Serializer):
    """ User login serializer """
    email = serializers.EmailField(
        required=True,
        allow_blank=True,
    )
    password = serializers.CharField(
        style={
            'input_type': 'password'
        }
    )

    def _validate_email(self, email, password):
        # checking provided details
        if email and password:
            user = authenticate(email=email, password=password)
        else:
            raise forms.ValidationError(
                _('Must include both email and password.')
            )
        return user

    def validate(self, attrs):
        email = attrs['email']
        password = attrs['password']

        user = self._validate_email(email, password)

        # if user cant be authenticated, return that some creds are wrong
        if not user:
            raise forms.ValidationError(
                _('Unable to login with provided credentials.')
            )

        # in case user has disabled his account...
        if not user.is_active:
            raise forms.ValidationError(
                _('Account is deactivated.')
            )

        attrs['user'] = user
        return attrs


class JWTSerializer(serializers.Serializer):
    """ jwt serializer """
    user = serializers.SerializerMethodField()
    token = serializers.CharField()

    def get_user(self, instance):
        # gets data from user serializer
        user_data = UserDetailsSerializer(instance['user']).data
        return user_data
