# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)


class UserManager(BaseUserManager):
    """ Extending djangos base user manager for our reqs """
    use_in_migration = True

    def _create_user(self, email, password, **extra_field):
        # processing method
        if not email:
            raise ValueError('Email is not given...')
        email = self.normalize_email(email)

        user = self.model(email=email, **extra_field)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        # normal user creation
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        # is active is used rather than deleting account
        # althou you can delete account if you want to
        extra_fields.setdefault('is_active', True)
        # return processed user
        return self._create_user(
            email, password, **extra_fields
        )

    def create_superuser(self, email, password, **extra_fields):
        # super user creation
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        # is active is used rather than deleting account
        extra_fields.setdefault('is_active', True)

        if not extra_fields.get('is_superuser'):
            raise ValueError('User must be part of superusers')

        if not extra_fields.get('is_staff'):
            raise ValueError('Superuser must be part of staff')

        # return processed user
        return self._create_user(
            email, password, **extra_fields
        )


class user(AbstractBaseUser, PermissionsMixin):
    """ Extending djangos abstract base user to fit our needs """
    # most of fields will be filled with enrichment data if data exists
    is_staff = models.BooleanField(
        _('Is Staff?'),
        default=False
    )
    is_superuser = models.BooleanField(
        _('Is SuperUser?'),
        default=False
    )
    is_active = models.BooleanField(
        _('Is Active?'),
        default=True
    )

    username = models.CharField(
        _('Username: '),
        max_length=150,
        unique=False,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'
        ),
        blank=True
    )

    email = models.EmailField(
        _('Email: '),
        # same email cannot be registered twice
        unique=True,
        help_text=_(
            'Required. Only accepts validated email addresses from emailhunter.'
        )
    )

    first_name = models.CharField(
        _('First Name: '),
        max_length=64,
        blank=True,
        help_text=_(
            'Users first name if provided, 64 chars or fewer.'
        )
    )

    last_name = models.CharField(
        _('Last Name: '),
        max_length=126,
        blank=True,
        help_text=_(
            'Users last name if provided, 126 chars or fewer.'
        )
    )

    full_name = models.CharField(
        _('Full Name: '),
        max_length=256,
        blank=True,
        help_text=_(
            'Users full name if provided, 256 chars of fewer.'
        )
    )

    company_name = models.CharField(
        _('Company Name: '),
        max_length=256,
        blank=True,
        help_text=_(
            'Users company name if provided, 256 chars of fewer.'
        )
    )

    avatar = models.URLField(
        _('Avatar: '),
        max_length=512,
        blank=True,
        help_text=_(
            'Users avatar if provided, i didnt wanted to use image obj cuz right now, '
            'getting image via requests would be too much delay time. '
            'String URL can be used as trusted URL further in frontend'
        )
    )

    location = models.CharField(
        _('Location: '),
        max_length=256,
        blank=True,
        help_text=_(
            'Users location if provided, 256 chars of fewer.'
        )
    )

    facebook = models.URLField(
        _('FB Profile: '),
        max_length=512,
        blank=True,
        help_text=_(
            'Users FB if provided, 512 chars or fewer.'
        )
    )

    twitter = models.URLField(
        _('Twitter: '),
        max_length=512,
        blank=True,
        help_text=_(
            'Users Twitter if provided, 512 chars or fewer.'
        )
    )

    created_at = models.DateTimeField(
        _('Account Created: '),
        auto_now_add=True,
    )

    last_login = models.DateTimeField(
        _('Last Login: '),
        auto_now=True,
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __unicode__(self):
        return self.email

    def __str__(self):
        return self.email

    def get_email(self):
        return self.email

    def get_username(self):
        if self.username:
            return self.username

    def get_short_name(self):
        if self.first_name:
            return self.first_name

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
