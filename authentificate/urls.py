# -*- coding: utf-8 -*-
from django.conf.urls import url
from authentificate.viewsets import (
    RegisterViewSet,
    LoginViewSet,
    UserDetailsViewSet
)


urlpatterns = [
    url(r'^register/$', RegisterViewSet.as_view(), name='register'),
    url(r'^login/$', LoginViewSet.as_view(), name='login'),
    url(r'^profile/$', UserDetailsViewSet.as_view(), name='profile'),
]
