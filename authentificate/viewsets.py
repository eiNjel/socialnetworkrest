# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model
from rest_framework import (
    generics,
    views,
    status,
    permissions,
    response
)
from authentificate.jwt import jwt_encode
from authentificate.serializers import (
    UserDetailsSerializer,
    UserRegistrationSerializer,
    LoginSerializer,
    JWTSerializer,
)

User = get_user_model()


class RegisterViewSet(generics.CreateAPIView):
    """ Registration API Endpoint """
    serializer_class = UserRegistrationSerializer
    permission_classes = [
        permissions.AllowAny,
    ]

    def create(self, request, *args, **kwargs):
        # create obj from requests data or get exception error
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        return response.Response(
            data=serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class LoginViewSet(views.APIView):
    """ Login API Endpoint """
    serializer_class = LoginSerializer
    permission_classes = [
        permissions.AllowAny,
    ]

    def post(self, request, created=False):
        # load serializer data from request
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        # get token for this user
        token = jwt_encode(user)

        # pass data to response if needed to set cookie later on
        details = JWTSerializer(
            instance={
                'user': user,
                'token': token
            },
            context={
                'request': request
            }
        ).data

        return response.Response(
            data=details,
            status=status.HTTP_200_OK
        )


class UserDetailsViewSet(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserDetailsSerializer
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get_object(self):
        # return currently authenticated user
        return self.request.user
