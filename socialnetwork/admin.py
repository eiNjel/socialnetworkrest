# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from socialnetwork.models import Post
from django.contrib import admin


class PostAdmin(admin.ModelAdmin):
    # slug field is already commited from model so no need to be able to edit it for now
    readonly_fields = ['slug']
    list_filter = ['title', 'slug']


admin.site.register(Post, PostAdmin)
