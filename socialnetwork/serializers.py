# -*- coding: utf-8 -*-
from rest_framework.serializers import ModelSerializer
from authentificate.serializers import UserDetailsSerializer
from socialnetwork.models import Post


class PostSerializer(ModelSerializer):
    author = UserDetailsSerializer(many=False, read_only=True)

    def update(self, instance, validated_data):
        # get data
        try:
            like = validated_data.get('like')
        except IndexError:
            like = None
        try:
            unlike = validated_data.get('unlike')
        except IndexError:
            unlike = None

        # set appropriate data to instance
        instance.title = validated_data.get('title')
        instance.content = validated_data.get('content')
        instance.description = validated_data.get('description')

        if like:
            instance.likes.add(self.context)
        else:
            # in case you have previously unliked this post and then user like it
            # user is removed from list
            for item in instance.unlikes.all():
                if item == self.context:
                    instance.unlikes.remove(item)
        if unlike:
            instance.unlikes.add(self.context)
        else:
            # in case you have previously liked this post and then user unlike it
            # user is removed from list
            for item in instance.likes.all():
                if item == self.context:
                    instance.likes.remove(item)

        instance.save()
        return instance

    class Meta:
        model = Post
        fields = ['id', 'author', 'likes', 'unlikes', 'title', 'description', 'content', 'slug']
