# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Post(models.Model):
    """ Post model """
    # author gets placed by registered user that creates this post
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )
    # this was easier method for me instead of using integer field
    # since it could have frontend some day and likes/unlikes dont need to be limited within bot
    likes = models.ManyToManyField(
        User,
        related_name='user_likes',
        blank=True
    )
    unlikes = models.ManyToManyField(
        User,
        related_name='user_unlikes',
        blank=True
    )
    title = models.CharField(
        _('Post Title: '),
        max_length=256,
        help_text=_(
            'Give this post a title :)'
        )
    )
    description = models.TextField(
        _('Post Description: ')
    )
    content = models.TextField(
        _('Post Content: ')
    )
    slug = models.SlugField(
        _('Post Slug: '),
        blank=True
    )

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    # if needed some day
    def get_absolute_url(self):
        return '/post/' + self.slug

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # providing slug field from post title without special chars/spaces
        slug = ''.join(e for e in self.title if e.isalnum())
        self.slug = slug.lower()
        super(Post, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'
