# -*- coding: utf-8 -*-
from django.conf.urls import (
    url,
    include
)
from rest_framework.routers import DefaultRouter
from socialnetwork.viewsets import PostViewSet


router = DefaultRouter()
router.register(
    r'posts',
    PostViewSet,
    base_name='posts'
)

urlpatterns = [
    url(r'^', include(router.urls)),
]
