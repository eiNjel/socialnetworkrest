# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import (
    viewsets,
    permissions,
    response,
    status
)
from django.utils.translation import ugettext_lazy as _
from socialnetwork.models import Post
from socialnetwork.serializers import PostSerializer


class PostViewSet(viewsets.ModelViewSet):
    """ Post API Endpoint """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]
    lookup_field = 'slug'

    def create(self, request, *args, **kwargs):
        # create post obj from request data
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        # call overrided perform create method
        self.perform_create(serializer)

        return response.Response(
            data={
                'details': _('Successfully created new Post.')
            },
            status=status.HTTP_201_CREATED
        )

    def perform_create(self, serializer):
        # set author of this post to this request user
        post = serializer.save(author=self.request.user)
        return post

    def update(self, request, *args, **kwargs):
        # pass data to serializer with context of user id
        serializer = self.serializer_class(data=request.data, context=request.user.id)
        # call update serializer
        serializer.update(self.get_object(), request.data)
        serializer.is_valid(raise_exception=True)

        return response.Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        # lets remove all likes from post obj
        for item in instance.likes.all():
            instance.likes.remove(item)

        # lets remove all unlikes from post obj
        for item in instance.unlikes.all():
            instance.unlikes.remove(item)

        instance.remove()

        return response.Response(
            data={
                'details': _('Post deleted successfully.'),
            },
            status=status.HTTP_204_NO_CONTENT
        )
